Correction files that are taken as input for Phys/DecayTreeTuple/src/TupleToolMultiplicity.cpp

For the Run 1 2013  pPb sample at sqrt(s_NN) = 5.02 TeV:
    - correction_pA_Merged_MCsim09k.root (forward)
    - correction_Ap_Merged_MCsim09k.root (backward)
    - correction_pp_MD_MCsim09d.root (pp reference)
For the Run2 2016 pPb Sample at sqrt(s_NN)= 8.16 TeV, not yet implemented for use in the TupleTool (different selection and data format):
    - merged_eff_4D_pPb_8TeV.root (forward)
    - merged_eff_4D_Pbp_8TeV.root (backward)
